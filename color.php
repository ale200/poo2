<html lang="es">
<head>
<title>Configuracion De Colores</title>

</head>

<body>
<section>
<article>
<form action="color.php" method="POST">
<legend><span>Configuracion de Celdas</legend></span>
<br><br>
<ul>
<li>
<div class="campo">
<label for="content">Nombre Completo:</label>
<input type="text" name="content" size="40" maxlength="250"/>
</div>
</li>
<br><br>
<li>
<div class="campo">
<label for="bgcolor">Color de Fondo:</label>
<select name="bgcolor">
<option value="Blue" selected="selected">Azul</option>
<option value="DarKGreen">Verde</option>
<option value="Red">Rojo</option>
<option value="Orange">Naranja</option>
<option value="Brown">Cafe</option>
</select>
</div>
</li>
<br><br>

<li>
<div class="campo">
<label for="fgcolor">Color de texto:</label>
<select name="fgcolor">
<option value="White" selected="selected">Blanco</option>
<option value="Black">Negro</option>
<option value="LightGrey">Gris</option>
<option value="LightGreen">Verde</option>
<option value="LightPink">Rosado</option>
<option value="Yellow">Amarillo</option>
</select>
</div>
</li>
<br><br>
<li>
<div class="campo">
<label for="font">Fuente:</label>
<select name="font">
<option value="Arial" selected="selected">Arial</option>
<option value="Verdana">Verdana</option>
<option value="Helvitica">Helvitica</option>
</select>
</div>
</li>
<br><br>
<li>
<div class="campo">
<label for="size">Tamano:</label>
<select name="size">
<option value="16px" selected="selected">16</option>
<option value="18px">18</option>
<option value="20px">20</option>
<option value="30px">30</option>
</select>
</div>
</li>
<br><br>
<li>
<div class="boton">
<input type="submit" name="enviar" id="enviar" value="Enviar"/>
</div>
</li>

</ul>

</form>
</article>
</section>
</body>

</html>

<?php

class claseCelda{
    //propiedades de las celdas
    private $colorFondo;
    private $colorLetra;
    private $tipoLetra;
    private $tamanoLetra;


    //metodo  contructor de la clase claseCelda
    function __construct($bgcolor, $fgcolor, $fontfam, $fontsize){
        $this->colorFondo = $bgcolor;
        $this->colorLetra =$fgcolor;
        $this->tipoLetra =$fontfam;
        $this->tamanoLetra =$fontsize;
    }

    //metodos para configurar la celda

    function pinta_celda($contenido){
        $celda = "<td style=\"font-family:{$this->tipoLetra};";
        $celda .="font-size:{$this->tamanoLetra};";
        $celda .="padding:12px 16px;";
        $celda .="background:{$this->colorFondo};";
        $celda .="color:{$this->colorLetra};\">\n";
        $celda .= utf8_decode($contenido)."\n";
        $celda .="</td>\n <br><br>";
        echo $celda;
    }
}

if (isset($_POST['enviar'])){
    $content = isset($_POST['content']) ? $_POST['content']:"";

    if($content ==""){
        $msg ="<h3>Debe escribir un nombre </h3>";
        $msg .="<a href=\"color.html\">Regresar al formulario</a> <br><br>";
        die($msg);
    }


    $bgcolor = isset($_POST['bgcolor']) ? $_POST['bgcolor']:"";
    
    $fgcolor = isset($_POST['fgcolor']) ? $_POST['fgcolor']:"";

    $fontfam = isset($_POST['font']) ? $_POST['font']:"";

    
    $fontsize = isset($_POST['size']) ? $_POST['size']:"";

    $nuevacelda = new claseCelda( $bgcolor, $fgcolor, $fontfam, $fontsize);
    echo "<table>\n<tr>\n";
    $nuevacelda->pinta_celda($content);
    echo "</tr>\n</table>\n <br><br>";
}





?>

